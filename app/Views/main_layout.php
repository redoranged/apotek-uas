<?php
    // Web Profiling
    $company_name = "20171320010 - Ridho Muhammad Galih";
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Apotek UAS | <?=$web_title?></title>

    <!-- CSS Assets -->
    <link rel="stylesheet" href="<?=base_url()?>/assets/bootstrap-4.5.0/css/bootstrap.min.css"/>
    <style>
        body {
            padding-top: 3rem;
        }

        main {
            padding: 1.5rem;
        }
        
        footer {
            background-color: #f5f5f5;
        }
    </style>

    <?= $this->renderSection('style') ?>
</head>
<body>
    <?= $this->renderSection('setup') ?>
    <header>
        <!-- Navbar -->
        <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
            <a class="navbar-brand" href="<?=base_url()?>/">Apotek UAS</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
    
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <?= $this->renderSection('navbar') ?>
            </div>
        </nav>
    </header>

    <!-- Body -->
    <main role="main">
        <div class="container">
            <nav aria-label="breadcrumb">
                <?= $this->renderSection('breadcrumb') ?>
            </nav>

            <?php if(!empty(session()->getFlashdata('success'))){ ?>
            <div class="alert alert-success alert-dismissible fade show">
                <?php echo session()->getFlashdata('success');?>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?php } ?>

            <?php if(!empty(session()->getFlashdata('info'))){ ?>
            <div class="alert alert-info alert-dismissible fade show">
                <?php echo session()->getFlashdata('info');?>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?php } ?>

            <?php if(!empty(session()->getFlashdata('warning'))){ ?>
            <div class="alert alert-warning alert-dismissible fade show">
                <?php echo session()->getFlashdata('warning');?>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?php } ?>
            
            <?= $this->renderSection('main') ?>
        </div>
    </main>

    <!-- Footer -->
    <footer class="mt-auto py-3">
        <div class="container">
            <span class="text-muted"><?=$company_name?> &copy;<?= date('Y');?></span>
        </div>
    </footer>

    <!-- Javascript Assets -->
    <script src="<?=base_url()?>/assets/jquery-3.5.1.min.js"></script>
    <script src="<?=base_url()?>/assets/bootstrap-4.5.0/js/bootstrap.bundle.min.js"></script>

    <?= $this->renderSection('script') ?>
</body>
</html>