<?= $this->extend('main_layout') ?>

<?= $this->section('style') ?>

<?= $this->endSection() ?>

<?= $this->section('setup') ?>
    <?php
        // Pengkondisian Isi dari Form Data
        $status = "create";
        $form_route = "/obat/create";
        $breadcrumb = "Baru";
        $form_data = [
            'kode_obat' => 'Otomatis Terisi',
            'kode_supplier' => '',
            'nama_obat' => '',
            'produsen' => '',
            'harga' => '',
            'stok' => '',
            'foto' => '',
        ];

        // Jika Edit
        if(isset($obat)){
            $status = "update";
            $form_route = "/obat/update/".$obat['kode_obat'];
            $breadcrumb = $obat['nama_obat'];
            $form_data = [
                'kode_obat' => $obat['kode_obat'],
                'kode_supplier' => $obat['kode_supplier'],
                'nama_obat' => $obat['nama_obat'],
                'produsen' => $obat['produsen'],
                'harga' => $obat['harga'],
                'stok' => $obat['stok'],
                'foto' => $obat['foto'],
            ];
        }
    ?>
<?= $this->endSection() ?>

<?= $this->section('navbar') ?>
    <ul class="navbar-nav mr-auto">
        <li class="nav-item">
            <a class="nav-link" href="<?=base_url()?>/">Home</a>
        </li>
        <li class="nav-item active">
            <a class="nav-link" href="<?=base_url()?>/obat">Obat <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="<?=base_url()?>/supplier">Supplier</a>
        </li>
    </ul>
<?= $this->endSection() ?>

<?= $this->section('breadcrumb') ?>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?=base_url()?>/">Home</a></li>
        <li class="breadcrumb-item"><a href="<?=base_url()?>/obat">Data Obat</a></li>
        <li class="breadcrumb-item active" aria-current="page"><?=$breadcrumb?></li>
    </ol>
<?= $this->endSection() ?>

<?= $this->section('main') ?>
    <div class="card">
        <div class="card-body">
            <form action="<?=base_url()?><?=$form_route?>" method="post" enctype="multipart/form-data">
                <div class="form-group">
                    <label for="kode_obat">Kode Obat</label>
                    <input type="text" name="kode_obat" value="<?=$form_data['kode_obat']?>" id="kode_obat" class="form-control" readonly/>
                </div>
                <div class="form-group">
                    <label for="kode_supplier">Supplier</label>
                    <select name="kode_supplier" id="kode_supplier" class="form-control" required>
                        <option value="" selected disabled>-- Supplier --</option>
                    <?php
                        foreach ($supplier as $key => $data) {
                    ?>
                        <option value="<?=$data['kode_supplier']?>" <?php if($form_data['kode_supplier'] == $data['kode_supplier']){echo 'selected';}else{echo '';} ?>><?=$data['nama_supplier']?></option>
                    <?php
                        }
                    ?>
                    </select>
                </div>
                <div class="form-group">
                    <label for="nama_obat">Nama Obat</label>
                    <input type="text" name="nama_obat" value="<?=$form_data['nama_obat']?>" id="nama_obat" class="form-control" required/>
                </div>
                <div class="form-group">
                    <label for="produsen">Produsen</label>
                    <input type="text" name="produsen" value="<?=$form_data['produsen']?>" id="produsen" class="form-control" required/>
                </div>
                <div class="form-group">
                    <label for="harga">Harga</label>
                    <input type="text" name="harga" value="<?=$form_data['harga']?>" id="harga" class="form-control" required/>
                </div>
                <div class="form-group">
                    <label for="stok">Stok</label>
                    <input type="number" name="stok" value="<?=$form_data['stok']?>" id="stok" class="form-control" required/>
                </div>
                <div class="form-group">
                    <label for="inputFoto">Foto</label>
                    <div class="row">
                        <div class="col-6">
                            <div class="custom-file" id="inputFoto">
                                <input type="file" accept="image/*" name="foto" value="/uploads/<?=$form_data['foto']?>" class="custom-file-input" id="foto">
                                <label class="custom-file-label" for="foto">Choose file</label>
                            </div>
                        </div>
                        <div class="col-6">
                            <img id="imagePreview" src="<?=base_url()?>/uploads/<?=$form_data['foto']?>" alt="Image Preview" style="width: 10rem;" />
                        </div>
                    </div>
                </div>
                <?php
                if($status == "create") {
                ?>
                    <button type="submit" class="btn btn-primary">Tambah</button>
                <?php
                }
                ?>

                <?php
                if($status == "update") {
                ?>
                    <button type="submit" class="btn btn-info">Ubah</button>
                <?php
                }
                ?>
            </form>
        </div>
    </div>
<?= $this->endSection() ?>

<?= $this->section('script') ?>
    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                
                reader.onload = function(e) {
                $('#imagePreview').attr('src', e.target.result);
                }
                
                reader.readAsDataURL(input.files[0]); // convert to base64 string
            }
        }

        $("#foto").change(function() {
            readURL(this);
        });
    </script>
<?= $this->endSection() ?>