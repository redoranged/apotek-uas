<?php namespace App\Controllers;

class Home extends BaseController
{
	public function index()
	{
		$data['web_title'] = 'Home';
		return view('main_page', $data);
	}

	//--------------------------------------------------------------------

}
