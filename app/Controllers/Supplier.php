<?php namespace App\Controllers;

use App\Models\SupplierModel;

class Supplier extends BaseController
{
    public function __construct()
    {
        $this->supplier = new SupplierModel();
    }

	public function index()
	{
        $data['web_title'] = 'Supplier';

        $data['supplier'] = $this->supplier->getSupplier();
		return view('supplier/home_page', $data);
    }
    
    public function tambah()
    {
        $data['web_title'] = 'Supplier';

        return view('supplier/form_page', $data);
    }

    public function edit($id)
    {
        $data['web_title'] = 'Supplier';

        $data['supplier'] = $this->supplier->getSupplier($id);
        return view('supplier/form_page', $data);
    }

    //--------------------------------------------------------------------
    
    // Function CRUD

    public function create()
    {
        // Membuat Array Collection
        $data = [
            'nama_supplier' => $this->request->getPost('nama_supplier'),
            'alamat_supplier' => $this->request->getPost('alamat_supplier'),
        ];

        $insert = $this->supplier->saveSupplier($data);

        if($insert)
        {
            session()->setFlashdata('success', 'Berhasil Menambahkan Data Supplier!');
            
            return redirect()->to(base_url('supplier')); 
        }
    }

    public function update($id)
    {
        // Membuat Array Collection
        $data = [
            'nama_supplier' => $this->request->getPost('nama_supplier'),
            'alamat_supplier' => $this->request->getPost('alamat_supplier'),
        ];

        $insert = $this->supplier->updateSupplier($data, $id);

        if($insert)
        {
            session()->setFlashdata('info', 'Berhasil Mengubah Data Supplier!');
            
            return redirect()->to(base_url('supplier')); 
        }
    }

    public function delete($id)
    {
        $delete = $this->supplier->deleteSupplier($id);

        if($delete)
        {
            session()->setFlashdata('warning', 'Berhasil Menghapus Data Supplier!');
            
            return redirect()->to(base_url('supplier'));
        }
    }
}
